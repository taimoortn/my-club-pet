import React from 'react';
import {SafeAreaView} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Navigation from './navigation';
import AppBar from './core-ui/AppBar';
import styles from './core-ui/Button/styles';

const App = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <NavigationContainer>
        <AppBar />
        <Navigation />
      </NavigationContainer>
    </SafeAreaView>
  );
};

export default App;
