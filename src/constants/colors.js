export const colors = {
  primary: '#d15420',
  lightPrimary: '#f6ddd1',
  secondary: '#d1e7ec',
  default: 'green',
  white: 'white',
  black: 'black',
  gray: '#e0e0e0',
  lightGray: '#f2f2f2',
  barColor: '#d1e7ec',
  lightGreen: '#e9efb7',
  darkGray: '#a9a9a9',
  icon: {
    purple: '#b255a3',
    orange: '#cc5256',
    alpha: '#0db6b6',
  },
  subTitle: '#474747',
};
