import React, {Fragment, useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../../../constants/colors';

const AccordianItem = ({icon, iconColor, label, children}) => {
  const [isOpen, setOpen] = useState(false);

  const handleToggle = () => setOpen(!isOpen);

  return (
    <Fragment>
      <TouchableOpacity style={styles.container} onPress={handleToggle}>
        <View style={styles.content}>
          <MaterialCommunityIcons name={icon} size={25} color={iconColor} />
          <Text style={styles.text}>{label}</Text>
        </View>
        <MaterialIcons
          name={`keyboard-arrow-${isOpen ? 'up' : 'down'}`}
          size={25}
          color={colors.black}
        />
      </TouchableOpacity>
      {isOpen && children}
    </Fragment>
  );
};

export default AccordianItem;
