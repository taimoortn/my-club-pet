import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../../../constants/colors';
import styles from './styles';

const AccordianPanelListItem = ({children, disabled, data}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.container}
      disabled={disabled}
      onPress={() =>
        navigation.navigate('history', {
          data,
        })
      }>
      <Text>{children}</Text>
      <MaterialIcons
        name="keyboard-arrow-right"
        size={20}
        color={colors.darkGray}
      />
    </TouchableOpacity>
  );
};

export default AccordianPanelListItem;
