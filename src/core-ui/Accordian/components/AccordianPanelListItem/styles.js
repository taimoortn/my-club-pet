import {StyleSheet} from 'react-native';
import {colors} from '../../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingHorizontal: 14,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 0.3,
    borderBottomColor: colors.darkGray,
  },
});

export default styles;
