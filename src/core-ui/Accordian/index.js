import React from 'react';
import {View, Text} from 'react-native';
import AccordianItem from './components/AccordianItem';
import AccordianItemPanel from './components/AccordianItemPanel';
import AccordianPanelListItem from './components/AccordianPanelListItem';
import styles from './styles';

const Accordian = ({children}) => {
  return (
    <View>
      <View style={styles.triangleContainer}>
        <View style={styles.triangle} />
      </View>
      <View style={styles.container}>{children}</View>
    </View>
  );
};

Accordian.Item = AccordianItem;

Accordian.ItemPanel = AccordianItemPanel;

Accordian.ItemPanelList = AccordianPanelListItem;

export default Accordian;
