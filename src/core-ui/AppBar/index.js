import React from 'react';
import {View, Image} from 'react-native';
import styles from './styles';
import {images} from '../../constants/images';

const Header = () => {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image source={images.headerImg} style={styles.size} />
      </View>
    </View>
  );
};

export default Header;
