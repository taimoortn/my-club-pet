import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    backgroundColor: colors.barColor,
  },
  imageContainer: {
    width: 200,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  size: {
    width: '70%',
    height: '70%',
  },
});

export default styles;
