import React, {Fragment} from 'react';
import {View, Image, Text} from 'react-native';
import styles from './styles';

const Avatar = ({size, title, subTitle, img}) => {
  return (
    <View style={styles().container}>
      <View style={styles({size}).imageContainer}>
        <Image source={img} style={styles().image} />
      </View>
      {Boolean(title) && <Text style={styles().title}>{title}</Text>}
      {Boolean(subTitle) && <Text style={styles().subTitle}>{subTitle}</Text>}
    </View>
  );
};

export default Avatar;
