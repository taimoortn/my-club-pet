import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors';

const styles = ({size = 'md'} = {}) =>
  StyleSheet.create({
    container: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    imageContainer: {
      width: size == 'xs' ? 50 : size == 'sm' ? 70 : 80,
      height: size == 'xs' ? 50 : size == 'sm' ? 70 : 80,
      borderRadius: 100,
      justifyContent: 'center',
      alignItems: 'center',
    },
    image: {
      flex: 1,
      width: '100%',
      height: '100%',
      resizeMode: 'cover',
      borderRadius: 100,
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      marginVertical: 1,
    },
    subTitle: {
      fontSize: 16,
      color: colors.subTitle,
    },
  });

export default styles;
