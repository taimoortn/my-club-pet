import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';

const Button = ({fullWidth, variant, disabled, children, onPress}) => {
  return (
    <TouchableOpacity
      style={styles({fullWidth, variant, disabled}).container}
      onPress={onPress}
      disabled={disabled}>
      <Text style={styles({variant, disabled}).text}>{children}</Text>
    </TouchableOpacity>
  );
};

export default Button;
