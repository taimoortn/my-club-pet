import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors';

const styles = ({
  fullWidth = false,
  variant = 'contained',
  disabled = false,
} = {}) =>
  StyleSheet.create({
    container: {
      width: fullWidth ? '100%' : '50%',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: disabled
        ? colors.gray
        : variant === 'contained'
        ? colors.primary
        : colors.white,
      padding: 12,
      borderRadius: 8,
      borderWidth: disabled ? 0 : variant === 'outlined' ? 1 : 0,
      borderColor: variant === 'outlined' ? colors.primary : colors.white,
    },
    text: {
      color: disabled
        ? colors.black
        : variant === 'outlined'
        ? colors.black
        : colors.white,
    },
  });

export default styles;
