import React from 'react';
import {View} from 'react-native';

const Divider = ({my = 20} = {}) => {
  return <View style={{marginTop: my ? my : 20}} />;
};

export default Divider;
