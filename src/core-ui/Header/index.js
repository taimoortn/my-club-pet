import React from 'react';
import {View, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../constants/colors';
import styles from './styles';

const Navigation = ({title}) => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <MaterialIcons
        name="keyboard-arrow-left"
        size={30}
        color={colors.black}
        onPress={() => navigation.goBack()}
      />
      {Boolean(title) && <Text style={styles.text}>{title}</Text>}
      <Text></Text>
    </View>
  );
};

export default Navigation;
