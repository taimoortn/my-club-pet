import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';

export const Tabs = ({children, disabled, activeTab: acTab, onChangeTab}) => {
  const [activeTab, setActiveTab] = useState(acTab || children[0].props.index);

  const handlePress = tab => {
    setActiveTab(tab);
    onChangeTab(tab);
  };

  return (
    <View style={styles({disabled}).container}>
      {children.map((child, i) => {
        const {index, label} = child.props;
        return (
          <Tab
            activeTab={activeTab}
            key={i}
            index={index}
            label={label}
            disabled={disabled}
            tabs={children.length || 0}
            onPress={handlePress}
          />
        );
      })}
    </View>
  );
};

export const Tab = ({activeTab, index, label, disabled, tabs, onPress}) => {
  const handlePress = () => onPress(index);

  return (
    <TouchableOpacity
      disabled={disabled}
      style={styles({active: activeTab === index, tabs}).tab}
      onPress={handlePress}>
      <Text>{label}</Text>
    </TouchableOpacity>
  );
};
