import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors';

const styles = ({active = false, disabled = false, tabs = 0} = {}) =>
  StyleSheet.create({
    container: {
      width: '100%',
      backgroundColor: disabled ? colors.lightGray : colors.lightPrimary,
      borderRadius: 8,
      padding: 6,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    tab: {
      width: 100 / tabs + '%',
      backgroundColor: active ? colors.white : 'transparent',
      padding: 5,
      borderRadius: 8,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default styles;
