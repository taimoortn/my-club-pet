import React from 'react';
import {View, Text, TextInput as RNTextInput} from 'react-native';
import styles from './styles';

const TextInput = ({label, value = '', onChangeText}, {}) => {
  return (
    <View style={styles({valid: value}).container}>
      {Boolean(label) && <Text style={styles().text}>{label}</Text>}
      <RNTextInput
        style={styles().input}
        value={value}
        onChangeText={onChangeText}
      />
    </View>
  );
};

export default TextInput;
