import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors';

const styles = ({valid = false} = {}) =>
  StyleSheet.create({
    container: {
      height: 55,
      marginTop: 20,
      backgroundColor: colors.white,
      borderBottomWidth: 1,
      borderBottomColor: valid ? colors.lightGreen : colors.gray,
    },
    input: {
      flex: 1,
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 5,
      fontSize: 18,
      padding: 5,
    },
    text: {
      color: colors.subTitle,
      marginBottom: 10,
    },
  });

export default styles;
