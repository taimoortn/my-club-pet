import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Home from '../screens/Home';
import Education from '../screens/Education';
import Help from '../screens/Help';
import Account from '../screens/Account';
import History from '../screens/Forms/History';
import AddPet from '../screens/Forms/AddPet';
import {colors} from '../constants/colors';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerShown: false,
        abBarStyle: { position: 'absolute' },
        tabBarActiveBackgroundColor: colors.barColor,
        tabBarInactiveBackgroundColor: colors.barColor,
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'My Pets') {
            iconName = focused ? 'home' : 'home';
          } else if (route.name === 'Education') {
            iconName = focused ? 'cast-education' : 'cast-education';
          } else if (route.name === 'Help') {
            iconName = focused ? 'help-circle-outline' : 'help-circle-outline';
          } else if (route.name === 'Account') {
            iconName = focused ? 'account' : 'account';
          }
          return (
            <MaterialCommunityIcons name={iconName} size={size} color={color} />
          );
        },
        tabBarActiveTintColor: colors.primary,
        tabBarInactiveTintColor: colors.black,
      })}>
      <Tab.Screen name="My Pets" component={HomeStackScreen} />
      <Tab.Screen name="Education" component={Education} />
      <Tab.Screen name="Help" component={Help} />
      <Tab.Screen name="Account" component={Account} />
    </Tab.Navigator>
  );
};

const HomeStackScreen = () => {
  return (
    <Stack.Navigator
      initialRouteName="home"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="home" component={Home} />
      <Stack.Screen name="history" component={History} />
      <Stack.Screen name="addPet" component={AddPet} />
    </Stack.Navigator>
  );
};

export default MainApp;
