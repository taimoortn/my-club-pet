import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Header from '../../../core-ui/Header';
import TextInput from '../../../core-ui/TextInput';
import {Tabs} from '../../../core-ui/Tabs';
import Button from '../../../core-ui/Button';
import Divider from '../../../core-ui/Divider';
import medications from '../../../constants/data/medications.json';
import styles from './styles';

const AddPet = ({route}) => {
  const {
    params: {data = {}, formData = {}},
  } = route;

  const [values, setValues] = useState({
    name: formData.name,
    notes: formData.notes,
    level: formData.level || 0,
  });

  const navigation = useNavigation();

  const isValidForm = values.name && values.notes;

  const handleChangeValue = ({value, name}) =>
    setValues({...values, [name]: value});

  const handleSubmit = () => {
    if (!isValidForm) return;

    if (Object.entries(formData).length > 0) {
      const index = medications.history.findIndex(
        history => history.id === formData.id,
      );

      medications.history.splice(index, 1);
    }
    medications.history.push({
      ...values,
      id: Math.floor(Math.random() * 16) * 5,
      category: data.id,
    });
    navigation.navigate('history', {data});
  };

  console.log(`janiiiii`, JSON.stringify(values, null, 4));

  return (
    <View style={styles.container}>
      <Header title={`Add ${data.title}`} />

      <Divider />

      <TextInput
        label="Name"
        value={values.name}
        onChangeText={value => handleChangeValue({value, name: 'name'})}
      />
      <TextInput
        label="Notes"
        value={values.notes}
        onChangeText={value => handleChangeValue({value, name: 'notes'})}
      />

      <Divider />

      <Text style={styles.text}>Severity level</Text>

      <Tabs
        disabled={!isValidForm}
        activeTab={values.level}
        onChangeTab={value => handleChangeValue({value, name: 'level'})}>
        {medications.levels.map(level => (
          <View label={level.name} index={level.id} key={level.id} />
        ))}
      </Tabs>

      <Divider my={30} />

      <Button fullWidth onPress={handleSubmit} disabled={!isValidForm}>
        Submit
      </Button>
    </View>
  );
};

export default AddPet;
