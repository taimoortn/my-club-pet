import {StyleSheet} from 'react-native';
import {colors} from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: colors.white,
  },
  text: {
    color: colors.subTitle,
    marginBottom: 5,
  },
});

export default styles;
