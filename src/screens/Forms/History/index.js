import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Avatar from '../../../core-ui/Avatar';
import Button from '../../../core-ui/Button';
import Header from '../../../core-ui/Header';
import {images} from '../../../constants/images';
import {colors} from '../../../constants/colors';

import medications from '../../../constants/data/medications.json';
import styles from './styles';

const Details = ({route}) => {
  const [history, setHistory] = useState([]);

  const {
    params: {data = {}},
  } = route;

  const navigation = useNavigation();

  useEffect(() => {
    setHistory(medications.history);
  }, [medications.history]);

  const filterHistory = history.filter(history => history.category === data.id);

  return (
    <View style={styles.container}>
      <Header />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Avatar title="Spot" subTitle={data.name} img={images.dog1} />

        <View style={styles.button}>
          <Button
            fullWidth
            onPress={() => navigation.navigate('addPet', {data})}>
            Add {data.title}
          </Button>
        </View>

        <View style={styles.list}>
          {filterHistory.length <= 0 ? (
            <Text style={styles.text}>No {data.name.toLowerCase()} added yet</Text>
          ) : (
            filterHistory.map((history, index) => (
              <TouchableOpacity
                style={styles.listItem}
                onPress={() =>
                  navigation.navigate('addPet', {
                    data,
                    formData: history,
                  })
                }>
                <Text>{history?.name}</Text>
                <MaterialIcons
                  name="keyboard-arrow-right"
                  size={20}
                  color={colors.black}
                />
              </TouchableOpacity>
            ))
          )}
        </View>
      </ScrollView>
    </View>
  );
};

export default Details;
