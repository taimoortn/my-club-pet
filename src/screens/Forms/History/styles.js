import {StyleSheet} from 'react-native';
import {colors} from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: colors.white,
  },
  button: {
    marginTop: 20,
  },
  list: {
    width: '100%',
    paddingVertical: 10,
  },
  listItem: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    marginTop: 40,
    textAlign: 'center',
    color:colors.subTitleƒ
  },
});

export default styles;
