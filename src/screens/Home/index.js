import React from 'react';
import {View, SafeAreaView, ScrollView} from 'react-native';
import Avatar from '../../core-ui/Avatar';
import Button from '../../core-ui/Button';
import Accordian from '../../core-ui/Accordian';
import {colors} from '../../constants/colors';
import {images} from '../../constants/images';
import medications from '../../constants/data/medications.json';
import subscriptionsData from '../../constants/data/subscriptions.json';
import photoGalleryData from '../../constants/data/photoGallery.json';
import styles from './styles';

const Home = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: 'space-between',
          flexDirection: 'column',
        }}
        style={{backgroundColor: 'white', paddingBottom: 20}}>
        <View style={styles.homeSection}>
          <View style={styles.topContainer}>
            <View style={styles.avatar}>
              <Avatar size="sm" title="Odie" img={images.dog2} />
            </View>

            <View style={styles.accordian}>
              <Accordian>
                <Accordian.Item
                  label="Medical history"
                  icon="clipboard-text-outline"
                  iconColor={colors.icon.purple}>
                  <Accordian.ItemPanel>
                    {medications.categories.map(category => (
                      <Accordian.ItemPanelList
                        key={category.id}
                        data={category}>
                        {category.name}
                      </Accordian.ItemPanelList>
                    ))}
                  </Accordian.ItemPanel>
                </Accordian.Item>
                <Accordian.Item
                  label="Subscription/Shop"
                  icon="shopping-outline"
                  iconColor={colors.icon.orange}>
                  <Accordian.ItemPanel>
                    {subscriptionsData.list.map(list => (
                      <Accordian.ItemPanelList key={list.id} disabled>
                        {list.name}
                      </Accordian.ItemPanelList>
                    ))}
                  </Accordian.ItemPanel>
                </Accordian.Item>
                <Accordian.Item
                  label="Photo gellery"
                  icon="file-image-outline"
                  iconColor={colors.icon.alpha}>
                  <Accordian.ItemPanel>
                    {photoGalleryData.list.map(list => (
                      <Accordian.ItemPanelList key={list.id} disabled>
                        {list.name}
                      </Accordian.ItemPanelList>
                    ))}
                  </Accordian.ItemPanel>
                </Accordian.Item>
              </Accordian>
            </View>
          </View>

          <View style={styles.bottomContainer}>
            <Button fullWidth onPress={() => alert('ok')}>
              Add Another Pet
            </Button>

            <Button fullWidth variant="outlined" onPress={() => alert('ok')}>
              Delete Pet
            </Button>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
