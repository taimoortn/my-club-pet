import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors';

const styles = StyleSheet.create({
  homeSection: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 15,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  topContainer: {},
  avatar: {
    marginTop: 10,
  },
  accordian: {
    marginTop: 10,
  },
  bottomContainer: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 100,
    marginTop: 20,
  },
});

export default styles;
